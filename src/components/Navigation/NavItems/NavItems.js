import React from 'react';
import styles from './NavItems.css';

import NavItem from './NavItem/NavItem';

const navItems = (props) => (
  <div className={styles.NavItems}>
    <NavItem path='/1'>Drinks</NavItem>
    <NavItem path='/search'>Search</NavItem>
  </div>
);


export default navItems;
