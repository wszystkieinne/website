import React from 'react';
import styles from './Toolbar.css';

import Logo from '../../Logo/Logo'
import NavItems from '../NavItems/NavItems'


const toolbar = (props) => (
  <div className={styles.Toolbar}>
    <Logo className={styles.Logo}/>
    <NavItems />
  </div>
);

export default toolbar;
