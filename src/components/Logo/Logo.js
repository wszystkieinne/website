import React from 'react';

import Logo from '../../assets/coctail-logo.png';
import styles from './Logo.css';

const logo = (props) => (
    <div className={styles.Logo}>
        <img src={Logo} alt="DrinkApp"/>
    </div>
);

export default logo;
