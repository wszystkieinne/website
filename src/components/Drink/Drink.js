import React from 'react';
import styles from  './Drink.css';

const drink = (props) => (
  <div onClick={props.clicked} className={styles.Drink}>
  <p>{props.name}</p>
  <img src={props.image} alt={props.image}  width="400" height="500"/>
  </div>
);

export default drink;
