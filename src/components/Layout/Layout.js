import React from 'react';
import styles from './Layout.css';

import Toolbar from '../Navigation/Toolbar/Toolbar'


const layout = (props) => (
  <div className={styles.Layout}>
    <Toolbar />
    <div className={styles.Children}>
      {props.children}
    </div>
  </div>
);



export default layout;
