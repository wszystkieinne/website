import React from 'react';
import styles from  './Button.css';
import {NavLink} from 'react-router-dom';


const button = (props) => <NavLink to={props.to}
  className={styles.Button} 
  activeClassName={styles.active}
  >{props.children}</NavLink>


export default button;
