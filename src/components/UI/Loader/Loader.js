import React from 'react';
import styles from './Loader.css';

const loader = (props) => (
  <div className={styles.Loader}>
    Loading...
  </div>
);


export default loader;
