import React from 'react';
import styles from './Pagger.css';

import Button from '../Button/Button';
import PageNumbers from './PageNumbers/PageNumbers';

const pagger = (props) =>{ 
  let next = null;
  let prev = null;
  props.nextactive ?  next = (+props.current + 1).toString() : next=('/'+props.current);
  props.prevactive ?  prev = (+props.current - 1).toString() : prev=('/'+props.current);
  
  return(
  <div className={styles.Pagger}>
    <Button disabled={props.prevactive} to={prev}>Prev</Button>
    <PageNumbers number={props.number}/>
    <Button disabled={props.nextactive} to={next}>Next</Button>
  </div>
)};


export default pagger;
