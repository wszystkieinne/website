import React from 'react';
//import { Test } from './PageNumbers.css';


import Button from '../../Button/Button';

const pageNumbers = (props) => {
  let pagenumbers = [];
  for (let i = 1; i<=props.number; i++){
    pagenumbers.push(i);
    console.log(pagenumbers);
  }
  pagenumbers = pagenumbers.map((el,i) =><Button key={i} to={'/' + el}>{el}</Button>)
  console.log(pagenumbers);
  return(
    <div >
    {pagenumbers}
    </div>
  );
};


export default pageNumbers;
