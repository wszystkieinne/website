import React, {Component} from 'react';
import styles from './App.css';
import {Route, Redirect, Switch} from 'react-router-dom';

import Layout from './components/Layout/Layout';
import DrinksList from './containers/DrinksList/DrinksList';
import DrinkInfo from './containers/DrinksList/DrinkInfo/DrinkInfo';
import SearchIng from './containers/SearchIng/SearchIng';

class App extends Component {
  render(){
    return (
    <div className={styles.App}>
      <Layout>
        <Switch>
          <Route path="/search/"  component={SearchIng}/>
          <Route path="/drink/:id"  component={DrinkInfo}/>
          <Route path="/:id" component={DrinksList}/>
          <Redirect from="/" to="/1" component={DrinksList}/>
        </Switch>
      </Layout>
    </div>
    );
}}

export default App;
