import React, { Component } from 'react';
import styles from './SearchIng.css';
import axios from '../../axios-config';

class SearchIng extends Component { 
  state ={
    input: '',
    name: null,
    description: null,
    search: false
  }

  componentDidMount = () => {
    console.log('SearchIng mounted');
  }

  componentDidUpdate = () => {
    console.log('SearchIng did update');
  }

  componentWillUnmount = () => {
    console.log('SearchIng will unmount');
  }

  searchButtonHandler = () =>{
    console.log('Im here');
    const name = this.state.input;
    axios.get('/json/v1/1/search.php?i=' + name)
      .then(Response =>{
        if(Response.data.ingredients){
        const Desc = Response.data.ingredients[0].strDescription;
        this.setState({description: Desc, name: name})
        } else {
        const not = "Not Found"
        this.setState({description: not, name: ''})
        }
      })
      .catch(error =>{
        console.log(error);
      })
  }

  render () {
    let ing = null;
    (this.state.description) ? ing=<div><h4>{this.state.name}</h4><p>{this.state.description}</p></div> : ing=null;
    return (
      <div className={styles.Search}>
      <label htmlFor="search">Search for ingredient: </label>
      <input 
      name="search" 
      type="text" 
      value={this.state.ingredient}
      onChange={(event) => this.setState({input: event.target.value})}
      />
      <button onClick={this.searchButtonHandler}>Search</button>
      {ing}
      </div>
    );
  }
}


export default SearchIng;
