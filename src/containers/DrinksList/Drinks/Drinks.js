import React, {Component} from 'react';
import axios from '../../../axios-config';
import styles from './Drinks.css';
import {withRouter} from 'react-router-dom';

import Loader from '../../../components/UI/Loader/Loader';
import Pager from '../../../components/UI/Pagger/Pagger';
import Drink from '../../../components/Drink/Drink';

class Drinks extends Component {
  state={
    drinks:[],
    currentpage: 0,
    numberofpages: 0,
    prevnotactive: false,
    nextnotactive: false,
    error: false
  }
  componentDidUpdate(){
    console.log('Drinks did Update');
    this.loadData();
  }
  

  componentDidMount(){
    console.log('Drinks did Mount');
    this.loadData();
  }

  loadData(){
    if(this.state.currentpage !== this.props.match.params.id){
    console.log(this.props);
    console.log();
    const current= this.props.match.params.id;
    let prev = false;    
    let next = false;
    let idstart=((current-1) * 20);
    let idend= idstart+20;
    axios.get('/json/v1/1/filter.php?c=Cocktail')
      .then(Response =>{
        const drinks = Response.data.drinks.slice(idstart,idend);
        let number = Object.keys(Response.data.drinks).length;
        console.log(number);
        number = Math.ceil(number/20);
        if(+current=== 1){prev=true};
        if(+current === number){next=true};
        console.log(number);
        const update = drinks.map((drink)=>{
          return  {...drink}
        });
        this.setState({
          drinks:update, 
          numberofpages: number, 
          currentpage:current,
          prevnotactive:prev,
          nextnotactive:next,
        });
      })
      .catch(error =>{
        console.log(error);
        this.setState({
          error: true
        })
      })
    }
  }

  clickDrinkHandler = (id) =>{
    console.log(this.props);
    this.props.history.push({pathname: "/drink/" +id});
  }

  render(){
    let drinks = null;
    if(!this.state.error){
      drinks = this.state.drinks.map((drink) =>{
        return(
          <Drink 
            key={drink.idDrink}
            name={drink.strDrink} 
            image={drink.strDrinkThumb}
            clicked={() => {this.clickDrinkHandler(drink.idDrink)}}
          />
        )
      });
    }  
    if (this.state.drinks.length === 0) {
      drinks = <Loader />;
    }
    if (this.state.error) {
      drinks = <p style={{textAlign:'center'}}>Something went wrong!</p>;
    }
    return(
      <div className={styles.Drinks}>
        {drinks}
        <Pager
        number={this.state.numberofpages}
        current={this.state.currentpage} 
        nextactive={!this.state.nextnotactive}
        prevactive={!this.state.prevnotactive}
        />
      </div>
)}};

export default withRouter(Drinks);
