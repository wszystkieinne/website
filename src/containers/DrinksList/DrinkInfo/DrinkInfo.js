import React, { Component } from 'react';
import axios from '../../../axios-config';
import styles from './DrinkInfo.css';

class DrinkInfo extends Component { 
state ={
  drink:{
  }
}

  componentDidMount = () => {
    console.log('DrinkInfo did mount');
    const id = this.props.match.params.id;
    axios.get('/json/v1/1/lookup.php?i='+ id)
      .then((Response)=>{
        const drink = Response.data.drinks[0];
        this.setState({drink:drink});
        console.log(this.state);
      })
      .catch((error)=>{
        console.log(error);
      })
  }

  componentWillUnmount = () => {
    console.log('DrinkInfo will unmount');
  }

  render () {

    return (
      <div className={styles.DrinkInfo}>
        <div className={styles.Top}>
          <img src={this.state.drink.strDrinkThumb} alt='Drink should be here'/>
          <table>
            <tbody>
                <tr>
                  <td><h2>{this.state.drink.strDrink}</h2></td>
                </tr><tr>
                  <td><strong>Instructions:</strong> {this.state.drink.strInstructions}</td>
                </tr>
              </tbody>
            </table>
        </div>
        <div className={styles.Boot}>
        <p><strong>Ingredients:</strong></p>
        <li>{this.state.drink.strMeasure1} {this.state.drink.strIngredient1}</li> 
        <li>{this.state.drink.strMeasure2} {this.state.drink.strIngredient2}</li>
        <li>{this.state.drink.strMeasure3} {this.state.drink.strIngredient3}</li>
        <li>{this.state.drink.strMeasure4} {this.state.drink.strIngredient4}</li>
        <li>{this.state.drink.strMeasure5} {this.state.drink.strIngredient5}</li>
        <li>{this.state.drink.strMeasure6} {this.state.drink.strIngredient6}</li>
        <li>{this.state.drink.strMeasure7} {this.state.drink.strIngredient7}</li>
        <li>{this.state.drink.strMeasure8} {this.state.drink.strIngredient8}</li>   
        </div>
      </div>
    );
  }
}


export default DrinkInfo;
