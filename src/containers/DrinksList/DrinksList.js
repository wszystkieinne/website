import React, { Component } from 'react';
import styles from './DrinksList.css';

import Drinks from './Drinks/Drinks';

class DrinksList extends Component { 
  state = {
    drinkType: ''
  } 

  componentDidMount = () => {
    console.log('DrinksList mounted');
  }

  componentWillUnmount = () => {
    console.log('DrinksList will unmount');
  }



  changePageHandler = () =>{

  }

  render () {
    return (
      <div className={styles.DrinksList}>
        <Drinks  />      
      </div>
    );
  }
}

export default DrinksList;
